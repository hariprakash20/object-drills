function invert(obj) {
    if(typeof obj === 'string') return {};
    let invertObj = {}
    for(let key in obj){
        invertObj[obj[key]] = key;
    }
    return invertObj;
}
module.exports = invert;
