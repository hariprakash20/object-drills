const keys = require('../keys.cjs');

const testObject1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const testObject2 = [1,2,3];

const testObject3 = null

const testObject4 = undefined;

const testObject5 = "String Test"

const testObject6 = () =>{ return 5};

console.log(keys(testObject1));
console.log(keys(testObject2));
console.log(keys(testObject3));
console.log(keys(testObject4));
console.log(keys(testObject5));
console.log(keys(testObject6));

