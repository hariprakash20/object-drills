const pairs = require('../pairs.cjs');

const testObject1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const testObject2 = [1,2,3];

const testObject3 = null

const testObject4 = undefined;

const testObject5 = "String test"

const testObject6 = () =>{return 5};


console.log(pairs(testObject1));
console.log(pairs(testObject2));
console.log(pairs(testObject3));
console.log(pairs(testObject4));
console.log(pairs(testObject5));
console.log(pairs(testObject6));