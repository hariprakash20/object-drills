const invert = require('../invert.cjs');

const testObject1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const testObject2 = [1,2,3];

const testObject3 = null

const testObject4 = undefined;

const testObject5 = "string test"

const func = () =>{}
const testObject6 = func;

console.log(invert(testObject1));
console.log(invert(testObject2));
console.log(invert(testObject3));
console.log(invert(testObject4));
console.log(invert(testObject5));
console.log(invert(testObject6));
