const values = require('../values.cjs');

const testObject1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham' }; 

const testObject2 = [1,2,3];

const testObject3 = null

const testObject4 = undefined;

const testObject5 = "String test"

const testObject6 = () =>{ return 5 };

console.log(values(testObject1));
console.log(values(testObject2));
console.log(values(testObject3));
console.log(values(testObject4));
console.log(values(testObject5));
console.log(values(testObject6));