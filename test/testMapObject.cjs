const mapObject = require('../mapObject.cjs');

function cb(val , key){
    return val + 5;
}

let testObject1 = {start: 5, end: 12};

 let testObject2 =[1,2,3];

 let testObject3 = null

let testObject4 = undefined;

let testObject5 = 0

console.log(mapObject(testObject1,cb));
console.log(mapObject(testObject2,cb));
console.log(mapObject(testObject3,cb));
console.log(mapObject(testObject4,cb));
console.log(mapObject(testObject5,cb));
console.log(mapObject("String test",cb));
console.log(mapObject(testObject1));