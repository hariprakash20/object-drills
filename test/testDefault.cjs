const defaults = require('../default.cjs');

const testObject1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };
const defaultProps1 = { name: 'Bruce Wayne', age: 36, location: 'Gotham', identity : "batman", occupation : "billionare" }

let testObject2 = {flavor: "chocolate",sprinkles: undefined};
let defaultProps2 = {flavor: "vanilla", sprinkles: "lots"}

const testObject3 = [1,3];
const defaultProps3 = [3,4,5];

const testObject4 = null
const defaultProps4 = null

const testObject5 = undefined;
const defaultProps5 = undefined;

const testObject6 = "String test"
const defaultProps6 = "Stiring test"

console.log(defaults(testObject1,defaultProps1));
console.log(defaults(testObject2,defaultProps2));
console.log(defaults(testObject3,defaultProps3));
console.log(defaults(testObject4,defaultProps4));
console.log(defaults(testObject5,defaultProps5));
console.log(defaults(testObject6,defaultProps6));
console.log(defaults(undefined,defaultProps1));
