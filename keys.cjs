function keys(obj) {
    if(typeof obj === 'string'){
        return [];
    }
    let keysArray = [];
    for(let key in obj){
        keysArray.push(key);
    }
    return keysArray;
}

module.exports = keys


