function mapObject(obj, cb) {
    if(typeof obj === 'string' || cb === undefined ){
        return {};
    }
    let res = {};
    for(let key in obj){
        res[key] = cb(obj[key],key); 
    }
    return res;
}

module.exports = mapObject;