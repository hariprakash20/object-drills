function defaults(obj, defaultProps) {
    if(obj === undefined && defaultProps === undefined 
        || obj === null && defaultProps === null 
        || obj === null && defaultProps ===undefined 
        || obj === undefined && defaultProps ===undefined) {
            return {};
        }
    if(Array.isArray(obj)) return Object.values({...defaultProps,...obj});
    if(typeof obj === 'string') return {...obj}
    if(obj === undefined) obj = {};
    for(let key in defaultProps){
        if(obj[key] === undefined)
        obj[key] = defaultProps[key];
    }
    return obj;
}

module.exports = defaults