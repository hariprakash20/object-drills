function values(obj) {
    if(typeof obj === 'string') return [];
    let value = []
    for(let key in obj){
        if(typeof obj[key] != 'function'){
            value.push(obj[key]);
        }
    }
    return value;
}

module.exports = values

